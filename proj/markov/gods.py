import base64
import random
from io import BytesIO

import numpy.random as np
from graphviz import Digraph
from PIL import Image

from proj.markov.name_generation import god_describe, god_name


class IdCounter:
    current_id = 0


class God:
    def __init__(self, name, father, mother, symbol, description, gender, generation, id_counter, base_branches):
        """
        The class representing a fake god.
        :param name: The god's name.
        :param father: The god's father.
        :param mother: The god's mother.
        :param symbol: The god's randomly generated rune.
        :param description: The god's randomly generated description.
        :param gender: The god's gender.
        :param generation: The god's generation
        :param base_branches: The 10 childs of the base gods
        """

        self.name = name
        self.father = father
        self.mother = mother
        self.children = []
        self.partner = None
        self.symbol = symbol
        self.description = description[0].lower() + description[1:]
        self.gender = gender
        self.generation = generation
        self.god_str = ""
        self.id_counter = id_counter
        self.db_id = id_counter.current_id
        self.base_branches = base_branches

    def __str__(self):
        """
        Generates a description of the god, based on the names of the children, his/her gender, the mother, the father
        and the randomly generated description from the markov chain.
        For example: Werdwon, daughter of Bixwolf and Runixisk, the virgin goddess of dominos pizza. Mother of Rrrpogen.
        :return: The description of the god
        """

        if self.god_str:
            return self.god_str

        son_daughter = "son" if self.gender == "male" else "daughter"
        if self.father is None and self.mother is not None:
            god_str = f"{self.name}, {son_daughter} of {self.mother.name}, {self.description}"
        elif self.father is not None and self.mother is None:
            god_str = f"{self.name}, {son_daughter} of {self.father.name}, {self.description}"
        else:
            if self.mother is self.father:
                god_str = f"{self.name}, {son_daughter} of {self.father.name}, {self.description}"
            else:
                god_str = f"{self.name}, {son_daughter} of {self.father.name} and {self.mother.name}, " \
                          f"{self.description}"

        if len(self.children) != 0:
            parent_type = "Father" if self.gender == "male" else "Mother"
            if len(self.children) > 1:
                child_list = ", ".join(child.name for child in self.children[:-1]) + " and " + self.children[-1].name
            else:
                child_list = self.children[0].name
            god_str += f" {parent_type} of {child_list}."

        return god_str

    def add_new_generation(self, gender, name="", parent=None):
        """
        Recursive method for adding the first god of a new generation.
        :param gender: The first god's gender
        :param name: The first god's name
        :param parent: The first god's parent, will be randomly generated if not given
        :return: The first god of a new generation
        """

        if not name:
            name = god_name(gender)

        if parent is None and self.partner is not None and random.uniform(0, 1) > 0.5:
            parent = self.partner

        if len(self.children) == 0:
            return self.add_child(gender, name, parent)
        else:
            return random.choice(self.children).add_new_generation(gender, name)

    def add_child(self, gender, name, parent=None):
        """
        Adds a new child to the current god.
        :param gender: Child's gender
        :param name: Child's name
        :param parent: Child's other parent, if not given it will be randomly generated
        :return: The child
        """

        if parent is None:
            parent = self.get_random_parent(gender)

        child_generation = self.generation if self.generation <= parent.generation else parent.generation
        child_generation += 1

        seed = np.randint(0, high=100000)
        self.id_counter.current_id += 1
        if self.gender == "male":
            child = God(name, self, parent, seed, god_describe(gender), gender, child_generation,
                        self.id_counter, self.base_branches)
        else:
            child = God(name, parent, self, seed, god_describe(gender), gender, child_generation,
                        self.id_counter, self.base_branches)

        if parent is not self:
            parent.children.append(child)
            self.children.append(child)
        else:
            self.children.append(child)

        return child

    def get_random_parent(self, gender):
        """
        Randomly walks the god graph to get a parent.
        :param gender: Parent's gender
        :return: A random parent from the graph
        """
        depth_list = {}
        for base_branch in self.base_branches:
            depth_list[base_branch] = get_progeny_amount(base_branch)

        base_god = min(depth_list, key=depth_list.get)

        max_walk_range = get_progeny_amount(base_god)
        if max_walk_range > 1:
            walk_range = random.randint(1, max_walk_range)
        else:
            return base_god

        parent = get_random_child_by_gender(gender, base_god)
        if parent is None:
            return base_god

        while walk_range > 0:
            new_parent = get_random_child_by_gender(gender, parent)
            if new_parent is None:
                return parent
            else:
                parent = new_parent
                walk_range -= 1

        return parent

    def to_json(self, mythology_id):
        god_json = {"name": self.name, "description": self.description, "gender": self.gender, "symbol": self.symbol,
                    "children": [child.db_id for child in self.children], "god_id": self.db_id,
                    "generation": self.generation, "mythology_id": mythology_id}
        if self.mother:
            god_json["mother"] = self.mother.db_id
        else:
            god_json["mother"] = None

        if self.father:
            god_json["father"] = self.father.db_id
        else:
            god_json["father"] = None

        return god_json


def get_random_child_by_gender(gender, parent):
    """
    Get a random child with a specific gender from parent
    :param gender: The child's gender
    :param parent: The parent to get the child from
    :return: A random child from the given parent with the given gender or None if no fitting child was found
    """

    if not any(child.gender == gender for child in parent.children):
        return None

    child = random.choice(parent.children)
    if child.gender != gender:
        return get_random_child_by_gender(gender, parent)

    return child


def add_tree_to_graph(node, graph, children_connections):
    if node.name not in children_connections:
        children_connections[node.name] = []
        graph.node(node.name, node.name)

    for child in node.children:
            graph.node(child.name, child.name)
            if child.name not in children_connections[node.name]:
                graph.edge(node.name, child.name)
                children_connections[node.name].append(child.name)
            add_tree_to_graph(child, graph, children_connections)


def walkup_parents(gender, current, steps):
    if steps > 0:
        if gender == "male" and current.father is None or gender == "female" and current.mother is None:
            return current
        else:
            return walkup_parents(gender, current, steps - 1)
    else:
        return current


def get_progeny_amount(current, counter=0):
    counter += len(current.children) + 1
    for child in current.children:
        counter += get_progeny_amount(child, counter)
    return counter


if __name__ == "__main__":
    rand = np.randint(0, high=100000)
    id_counter = IdCounter()
    base_god_list = []

    id_counter.current_id += 1
    root = God("Bixwolf", None, None, "Bixwolf.png", god_describe("male"), "male", 1, id_counter, base_god_list)
    root.god_str = "Bixwolf - The all knowing all father of the universe as we know it"

    id_counter.current_id += 1
    root.partner = God("Runixisk", None, None, "", god_describe("female"), "female", 1, id_counter, base_god_list)
    root.partner.god_str = "Runixisk - The all knowing all mother of the universe as we know it"

    id_counter.current_id += 1
    root2 = God("Bixwolf2", None, None, "", god_describe("male"), "male", 1, id_counter, base_god_list)
    root2.god_str = "Bixwolf2"
    id_counter.current_id += 1
    root2.partner = God("Runixisk2", None, None, "", god_describe("female"), "female", 1, id_counter, base_god_list)
    root2.partner.god_str = "Runixisk2"

    root_male_list = [root, root2]
    root_female_list = [root.partner, root2.partner]

    for _ in range(0, 10):
        gender = random.choice(["male", "female"])
        father = random.choice(root_male_list)
        mother = random.choice(root_female_list)
        base_god_list.append(father.add_child(gender, god_name(gender), mother))

    for _ in range(0, 10):
        gender = random.choice(["male", "female"])
        father = random.choice(root_male_list)
        child = father.add_new_generation(gender)
        base_god_list.append(child)

    for god in base_god_list:
        print(str(god))

    children_connections = {}

    graph = Digraph()

    # Having trouble changing the font colour here
    graph.attr(bgcolor='transparent')
    add_tree_to_graph(root, graph, children_connections)
    add_tree_to_graph(root2, graph, children_connections)
    add_tree_to_graph(root.partner, graph, children_connections)
    add_tree_to_graph(root2.partner, graph, children_connections)

    # This whole section really needs work
    # Specifically handle it as a string rather than file
    graph.format = 'png'
    graph.render('test-output/round-table.gv', view=True)

    # Stole a texture from a game, but not a good one
    bg_image = Image.open("block.jpg")
    bg_width, bg_height = bg_image.size

    god_image = Image.open("test-output/round-table.gv.png")

    width, height = god_image.size

    new_image = Image.new('RGBA', (width, height))

    for x in range(0, width, bg_width):
        for y in range(0, height, bg_height):
            new_image.paste(bg_image, (x, y))

    image_buffer = BytesIO()

    new_image = Image.alpha_composite(new_image, god_image)
    new_image.save(image_buffer, format("PNG"))

    family_tree_string = base64.b64encode(image_buffer.getvalue())

    print(family_tree_string)
