import markovify
from nomine import Nomine


def god_describe(gender):
    """
    Uses markovify to generate random descriptions
    Chains are fed using gender based descriptor texts
    :param gender: Accepts Female / Not Female
    :return: A lovely description
    """

    if gender == "female":
        descriptors = "proj/markov/descript_female.txt"
    else:
        descriptors = "proj/markov/descript_male.txt"

    with open(descriptors) as f:
        descriptions = f.read()

    # Pass descriptions to Markovify
    markov_describe = markovify.Text(descriptions)
    god_description = markov_describe.make_sentence()

    if god_description is None:
        return god_describe(gender)

    return god_description


def god_name(gender):
    """
    Uses Nomine to generate random names based off a pair of gender based name lists
    :param gender: Accepts Female / Not Female
    :return: A lovely god name
    """
    if gender == "female":
        result = Nomine(preset='gods_female').get()
    else:
        result = Nomine(preset='gods_male').get()

    if len(result) < 2:
        return god_name(gender)

    return result
