import base64
import logging
import random
import sys

import numpy.random as np
import rethinkdb as r
from flask import Flask, abort, jsonify, request
from graphviz import Digraph

from proj.markov.gods import God, IdCounter, add_tree_to_graph
from proj.markov.name_generation import god_describe, god_name
from proj.runes.runes import generate_rune


root = logging.getLogger()
root.setLevel(logging.DEBUG)

# logger to stdout
stdout_logger = logging.StreamHandler(sys.stdout)
# format for the stdout logger
logging.basicConfig(level=logging.DEBUG, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                    handlers=[stdout_logger])


log = logging.getLogger(__name__)
app = Flask(__name__)
conn = r.connect("localhost", 28015, db="mythology")

# Setup the database structure, if it has not been set up yet.
if "mythology" not in r.db_list().run(conn):
    r.db_create("mythology").run(conn)
if "gods" not in r.db("mythology").table_list().run(conn):
    r.db("mythology").table_create("gods").run(conn)


def god_from_dictionary(god_dict, mythology_id):
    """
    Recreates a god as good as possible from a god dict, the returned god is not able to add new children etc and just
    meant to be used by the API in conjunction with the god database.
    :param god_dict: The god as dictionary
    :param mythology_id: The god's mythology id
    :return: A not fully functional god object
    """

    god = God(god_dict["name"], god_dict["father"], god_dict["mother"], god_dict["symbol"], god_dict["description"],
              god_dict["gender"], god_dict["generation"], IdCounter(), None)

    cursor = r.table("gods").filter(r.row["mythology_id"] == mythology_id).run(conn)
    for child in cursor:
        if child["god_id"] in god_dict["children"]:
            god.children.append(god_from_dictionary(child, mythology_id))

    return god


@app.route("/mythologies", methods=["GET"])
def get_mythologies():
    """
    Get a list of mythologies in the db
    :return: List of mythologies
    """

    mythologies = []
    for mythology in r.table("gods").get_field("mythology_id").run(conn):
        mythologies.append(mythology)
    mythologies = set(mythologies)
    return jsonify(list(mythologies))


@app.route("/mythologies/<int:mythology_id>", methods=["GET"])
def get_mythology(mythology_id):
    """
    Get a list of all gods inside a mythology
    :param mythology_id: The id of the mythology
    :return: List of gods inside the mythology
    """

    gods = []
    for god in r.table("gods").filter(r.row["mythology_id"] == mythology_id).run(conn):
        gods.append(god)

    if not gods:
        abort(404)

    return jsonify(gods)


@app.route("/mythologies/<int:mythology_id>/<int:god_id>", methods=["GET"])
def get_god_by_mythology(mythology_id, god_id):
    gods = r.table("gods").filter(r.row["mythology_id"] == mythology_id).run(conn)
    for god in gods:
        if god["god_id"] == god_id:
            return jsonify(god)
    abort(404)


@app.route("/mythologies", methods=["POST"])
def create_mythology():
    """
    Creates a completely random mythology.
    :return: The new mythology id
    """

    count = request.json["god_amount"]
    # Limit the maximum amount of gods per mythology to 22 and set the minimum to 4.
    if count > 24 or count < 4:
        abort(403)
    cursor = r.table("gods").get_field("mythology_id").run(conn)
    mythology_id = list(set([mythology_id for mythology_id in cursor]))
    if not mythology_id:
        mythology_id = 1
    else:
        mythology_id = max(mythology_id) + 1

    god_list = []
    id_counter = IdCounter()

    root1 = God(god_name("male"), None, None, np.randint(0, high=100000), god_describe("male"), "male", 1, id_counter,
                god_list)
    id_counter.current_id += 1

    root1_female = God(god_name("female"), None, None, np.randint(0, high=100000), god_describe("female"), "female",
                       1, id_counter, god_list)
    id_counter.current_id += 1

    root2 = God(god_name("male"), None, None, np.randint(0, high=100000), god_describe("male"), "male", 1, id_counter,
                god_list)
    id_counter.current_id += 1

    root2_female = God(god_name("female"), None, None, np.randint(0, high=100000), god_describe("female"), "female",
                       1, id_counter, god_list)
    id_counter.current_id += 1

    root_male_list = [root1, root2]
    root_female_list = [root1_female, root2_female]
    count -= 4

    base_children_amount = count // 2
    for _ in range(0, base_children_amount):
        if count == 0:
            pass  # return later
        else:
            gender = random.choice(["male", "female"])
            father = random.choice(root_male_list)
            mother = random.choice(root_female_list)
            god_list.append(father.add_child(gender, god_name(gender), mother))
            count -= 1

    for _ in range(0, count):
        if count == 0:
            pass  # return later
        else:
            gender = random.choice(["male", "female"])
            father = random.choice(root_male_list)
            child = father.add_new_generation(gender)
            god_list.append(child)
            count -= 1

    god_list += root_female_list + root_male_list
    god_list = [god.to_json(mythology_id) for god in god_list]
    r.table("gods").insert(god_list).run(conn)
    return jsonify({"mythology_id": mythology_id})


@app.route("/mythologies/<int:mythology_id>/tree", methods=["GET"])
def get_tree_by_mythology(mythology_id):
    """
    Restores a mythology family tree and plots it.
    :param mythology_id: The id of the mythology to restore
    :return: Base64 encoded version of the family tree
    """

    base_gods = []
    cursor = r.table("gods").filter(r.row["mythology_id"] == mythology_id).run(conn)
    for base_god in cursor:
        if base_god["generation"] == 1:
            base_gods.append(god_from_dictionary(base_god, mythology_id))

    children_connections = {}
    graph = Digraph(graph_attr={'bgcolor': 'transparent'}, format="png")
    for base_god in base_gods:
        add_tree_to_graph(base_god, graph, children_connections)
    return jsonify({"tree": base64.b64encode(graph.pipe()).decode()})


@app.route("/images/<int:seed>", methods=["GET"])
def generate_image(seed):
    """
    Generates a rune for a given seed.
    :param seed: The seed
    :return: The rune as base64
    """

    image = generate_rune(seed)
    return jsonify({"image": image.decode()})
