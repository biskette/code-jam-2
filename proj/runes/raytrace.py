import math
from math import pi
# Code written by HOWZ1T


def to_degree(radian):
    return radian * (180 / pi)


def to_radian(degree):
    return degree * (pi / 180)


def get_angle(gradient):
    angle = to_degree(math.atan(gradient))
    while angle < 0:  # ensures positive angle
        angle += 360

    return angle


def get_gradient_from_points(point1, point2):
    x1 = point1[0]
    y1 = point1[1]
    x2 = point2[0]
    y2 = point2[1]

    x_dif = (x2 - x1)
    y_dif = (y2 - y1)

    m = 0
    if x_dif != 0:
        m = y_dif / x_dif

    return m


def get_gradient_from_angle(angle):
    return math.tan(to_radian(angle))


def get_angle_between_two_lines(gradient1, gradient2):
    a = gradient2 - gradient1
    b = 1 + gradient2 * gradient1
    if b == 0:
        b = 0.1
    angle = to_degree(math.atan(a / b))

    while angle < 0:  # ensures positive angle
        angle += 360

    return angle


def get_refraction_angle(angle):
    a = 180 - angle
    while a < 0:
        a += 360
    return a


def get_ray_cast_point(origin, angle, radius):
    hit = []
    hit_x = origin[0] + radius * math.cos(angle * math.pi / 180)
    hit_y = origin[1] + radius * math.sin(angle * math.pi / 180)
    hit.append(hit_x)
    hit.append(hit_y)
    return hit
