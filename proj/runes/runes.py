import base64
import logging
import math
from enum import IntEnum
from io import BytesIO


import numpy.random as np
from PIL import Image, ImageDraw


log = logging.getLogger(__name__)


class RuneType(IntEnum):
    quadrant = 0
    horizontal = 1
    vertical = 2


def draw_line_aa(size, point1, point2, width, fill, bglayer):
    """
    Draws an antiliased line and return Pil.Image
    :param size: Size of the image we're working with
    :param point1: Start point
    :param point2: End point
    :param width: Thickness of line
    :param fill: Colour
    :param bglayer: Layer we're going to draw on
    """
    im = Image.new('RGBA', (size * 2, size * 2), (255, 255, 255, 0))
    draw = ImageDraw.Draw(im)
    draw.line((point1[0]*2, point1[1]*2, point2[0]*2, point2[1]*2), fill=fill, width=width*2)
    im = im.resize((im.width//2, im.height//2), Image.ANTIALIAS)
    bglayer.paste(im, (0, 0), im)


def draw_circle_aa(size, point1, point2, fill, bglayer):
    """
    Draw an antiliased circle and return Pil.Image
    :param size: Size of the image we're working with
    :param point1: Start point
    :param point2: End point
    :param fill: Colour
    :param bglayer: Layer we're going to draw on
    """
    im = Image.new('RGBA', (size * 2, size * 2), (255, 255, 255, 0))
    draw = ImageDraw.Draw(im)
    draw.ellipse((point1[0]*2, point1[1]*2, point2[0]*2, point2[1]*2), fill=fill)
    im = im.resize((im.width//2, im.height//2), Image.ANTIALIAS)
    bglayer.paste(im, (0, 0), im)


def normalize_co_ords(maxsize, number_to_generate):
    """
    Generate random co-ordinates for our lines and normalize them
    :param maxsize: maximum size we have to work with
    :param number_to_generate: amount of co-ordinates to generate
    :return: returns our co-ordinates
    """
    border = maxsize / 100 * 10
    co_ords = np.randint(0, maxsize, size=(number_to_generate, 2))
    co_ords = [[k if k < maxsize - border else maxsize for k in x] for x in co_ords]
    co_ords = [[k if k > border else k+border for k in x] for x in co_ords]
    return co_ords


def line_distance(p, q):
    """
    Calculates the line distance in pixels between 2 points
    :param p: First co-ordinate
    :param q: Seconds co-ordinate
    :return: returns the length of line in pixels
    """
    dx = p[0] - q[0]
    dy = p[1] - q[1]
    dist = math.sqrt(dx*dx + dy*dy)
    return int(dist)


def normalize_rectangle_coords(max_x, max_y, number_to_generate):
    """
    Generate random co-ordinates for a rectangle and normalize them
    :param max_x: maximum size we have to work with
    :param max_y: maximum size we have to work with
    :param number_to_generate: amount of co-ordinates to generate
    :return: returns our co-ordinates
    """
    co_ords = []
    x_co_ords = np.randint(0, max_x, size=number_to_generate)
    y_co_ords = np.randint(0, max_y, size=number_to_generate)
    for i in range(number_to_generate):
        co_ords.append((x_co_ords[i], y_co_ords[i]))
    return co_ords


def generate_rune(file_name: str, seed: int=None, size: int=400, line_width: int=8):
    """
    # Main Rune Generation Code
    :param file_name: filename to save rune
    :param seed: the seed we are using to create the rune
    :param size: max size of the rune
    :param line_width: size of the lines for the rune
    """
    if seed is not None:
        np.seed(seed)

    mask_layer = Image.new('RGBA', (size, size), (255, 255, 255, 0))

    # Generate random co-ordinates for our lines
    rune_type = RuneType(np.randint(0, 3))
    if rune_type == RuneType.quadrant:
        co_ords = normalize_co_ords(size / 2, np.randint(3, 7))
    elif rune_type == RuneType.horizontal:
        co_ords = normalize_rectangle_coords(size, size / 2, np.randint(5, 9))
    elif rune_type == RuneType.vertical:
        co_ords = normalize_rectangle_coords(size / 2, size, np.randint(5, 9))

    # Start our initial line
    log.debug(line_distance(co_ords[0], co_ords[1]))
    draw_line_aa(size, co_ords[0], co_ords[1], line_width, (0, 0, 0, 255), mask_layer)

    # Draw the rest of our lines
    for i in range(2, len(co_ords)):
        log.debug(line_distance(co_ords[i-1], co_ords[i]))
        draw_line_aa(size, co_ords[i-1], co_ords[i], line_width, (0, 0, 0, 255), mask_layer)

    circle_co_ords = np.randint(0, size / 2, size=(1, 2))
    circle_co_ords = [(circle_co_ords[0][0], circle_co_ords[0][1]),
                      (circle_co_ords[0][0] + line_width * 2, circle_co_ords[0][1] + line_width * 2)]

    draw_circle_aa(size, circle_co_ords[0], circle_co_ords[1], (0, 0, 0, 255), mask_layer)

    # Check to see if we should join back to the initial line
    rand = np.randint(0, 2)
    if rand == 0:
        draw_line_aa(size, co_ords[len(co_ords) - 1], co_ords[0], line_width, (0, 0, 0, 255), mask_layer)

    # Add another random line (not attached)
    co_ords = normalize_co_ords(size / 2, 2)
    draw_line_aa(size, co_ords[0], co_ords[1], line_width, (0, 0, 0, 255), mask_layer)

    # Flip Images 0 or Rotate Images 1
    rand = np.randint(0, 2)
    if rune_type == RuneType.quadrant:
        if rand == 0:
            img_lr = mask_layer.transpose(Image.FLIP_LEFT_RIGHT)
            mask_layer.paste(img_lr, (0, 0), img_lr)
            img_tb = mask_layer.transpose(Image.FLIP_TOP_BOTTOM)
            mask_layer.paste(img_tb, (0, 0), img_tb)
        elif rand == 1:
            img_lr = mask_layer.rotate(-90)
            mask_layer.paste(img_lr, (0, 0), img_lr)
            img_lr = mask_layer.rotate(180)
            mask_layer.paste(img_lr, (0, 0), img_lr)
    elif rune_type == RuneType.horizontal:
        if rand == 0:
            img_tb = mask_layer.transpose(Image.FLIP_TOP_BOTTOM)
            mask_layer.paste(img_tb, (0, 0), img_tb)
        elif rand == 1:
            img_lr = mask_layer.rotate(180)
            mask_layer.paste(img_lr, (0, 0), img_lr)
    elif rune_type == RuneType.vertical:
        if rand == 0:
            img_lr = mask_layer.transpose(Image.FLIP_LEFT_RIGHT)
            mask_layer.paste(img_lr, (0, 0), img_lr)
        elif rand == 1:
            img_lr = mask_layer.rotate(-180)
            mask_layer.paste(img_lr, (0, 0), img_lr)

    # Center Rune onto background
    background_layer = Image.open('proj/runes/Stone.png')
    transparent_layer = Image.new('RGBA', background_layer.size, (255, 255, 255, 0))
    mask_layer = mask_layer.resize((int(mask_layer.width / 100 * 80), int(mask_layer.height / 100 * 80)))
    img_w, img_h = mask_layer.size
    bg_w, bg_h = background_layer.size
    offset = ((bg_w - img_w) // 2, (bg_h - img_h) // 2)

    # Paste onto our background layer
    transparent_layer.paste(mask_layer, offset, mask_layer)
    background_layer.paste(transparent_layer, None, transparent_layer)
    # background_layer = Image.blend(background_layer, transparent_layer, 0.4)

    # Stringify the image
    image_buffer = BytesIO()
    background_layer.save(image_buffer, format="PNG")
    image_string = base64.b64encode(image_buffer.getvalue())

    return image_string


if __name__ == "__main__":
    rand = np.randint(0, high=100000)
    print(f"Seed for this beautiful picture is {rand}")
    generate_rune("test_rune.png", rand, 400, 8)
